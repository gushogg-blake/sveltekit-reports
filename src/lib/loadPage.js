import createPage from "$lib/createPage";

export default async function(slug, fetch) {
	let importPath = await (await fetch("/api/get-import-path/" + slug)).json();
	
	importPath = importPath.replace(/\.\w+$/, "");
	
	let parts = importPath.substr("/src/pages/".length).split("/");
	let module;
	
	if (parts.length === 1) {
		module = await import(`../pages/${parts[0]}.md`);
	} else if (parts.length === 2) {
		module = await import(`../pages/${parts[0]}/${parts[1]}.md`);
	} else if (parts.length === 3) {
		module = await import(`../pages/${parts[0]}/${parts[1]}/${parts[2]}.md`);
	} else if (parts.length === 4) {
		module = await import(`../pages/${parts[0]}/${parts[1]}/${parts[2]}/${parts[3]}.md`);
	} else if (parts.length === 5) {
		module = await import(`../pages/${parts[0]}/${parts[1]}/${parts[2]}/${parts[3]}/${parts[4]}.md`);
	}
	
	return createPage(importPath + ".md", module);
}
