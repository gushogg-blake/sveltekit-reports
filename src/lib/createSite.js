import mapArrayToObject from "$utils/mapArrayToObject";

function createRepo(all) {
	let byId = mapArrayToObject(all, page => [page.id, page]);
	let bySlug = mapArrayToObject(all, page => [page.slug, page]);
	
	return {
		all() {
			return all;
		},
		
		byId(id) {
			return byId[id];
		},
		
		bySlug(slug) {
			return bySlug[slug];
		},
		
		_pagesByCat(list, path) {
			if (path === "*") {
				return list;
			} else if (path === null) {
				return list.filter(page => !page.cat);
			} else {
				return list.filter(page => page.cat === path);
			}
		},
		
		pagesByCat(path="*") {
			return this._pagesByCat(pages, path);
		},
		
		link(id) {
			return byId[id].href;
		},
	};
}

export default function(_all) {
	let all = _all.filter(page => !page.isIndex);
	
	return createRepo(all);
}
