import createSite from "$lib/createSite";

export default async function(fetch) {
	return createSite(await (await fetch("/api/list-pages")).json());
}
