import slugify from "slugify";
import fs from "fs";

function createPage(importPath, module) {
	let meta = module.metadata;
	let content = module.default;
	let [ext] = importPath.match(/\.\w+$/);
	//let type = ext.substr(1);
	let path = importPath.slice("/src/pages".length, -ext.length);
	let isUtil = !meta;
	
	if (!meta) {
		meta = {};
	}
	
	/*
	figure out details from filename etc, with overrides in meta
	
	e.g. id defaults to basename; slug defaults to id; link defaults to title
	*/
	
	let name = path.split("/").at(-1);
	let id = meta.id || name;
	let title = meta.title || name;
	let slug = slugify(meta.slug || id);
	let link = meta.link || title;
	let href = "/p/" + slug;
	let large = ("large" in meta) ? meta.large : id.endsWith(" index");
	let col = ("col" in meta) ? meta.col : true;
	let {date} = meta;
	let isPage = ("page" in meta) ? meta.page : !date;
	let isPost = !isPage;
	
	return {
		...meta,
		title,
		importPath,
		isUtil,
		id,
		slug,
		path,
		href,
		link,
		large,
		col,
		content,
		isPost,
		isPage,
	};
}

export default createPage;
