import mapArrayToObject from "$utils/mapArrayToObject";
import createPage from "$lib/createPage";

let files = import.meta.glob(["/src/pages/**/*.md"], {eager: true});
let all = Object.entries(files).map(([path, module]) => createPage(path, module)).filter(page => !page.isUtil);
let bySlug = mapArrayToObject(all, page => [page.slug, page]);

export default {
	all,
	bySlug,
};
