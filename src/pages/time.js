import groupBy from "$utils/array/groupBy";

export function formatTime(time) {
	return time.substr(0, 2) + ":" + time.substr(2);
}

export function formatMinutes(minutes) {
	return Math.floor(minutes / 60) + ":" + pad(Math.floor(minutes % 60));
}

export function pad(m) {
	return "0".repeat(2 - String(m).length) + m;
}

export function getGroupedEntries(lines) {
	let entries = [];
	let date;
	let hour;
	let lastEntryEndTime;
	let firstDate = null;
	let lastDate;
	
	function getExplicitMinutes(line) {
		if (line.match(/ = \d+h$/)) {
			let [, hours] = line.match(/(\d+)h$/);
			
			return Number(hours) * 60;
		} else if (line.match(/ = \d+h\d+$/)) {
			let [, hours, minutes] = line.match(/(\d+)h(\d+)$/);
			
			return Number(hours) * 60 + Number(minutes);
		} else if (line.match(/ = \d+m$/)) {
			let [, minutes] = line.match(/(\d+)m$/);
			
			return Number(minutes);
		}
		
		return null;
	}
	
	for (let line of lines) {
		if (line.match(/^\d+\/\d+$/)) { // date
			date = line;
			
			if (!firstDate) {
				firstDate = line;
			}
			
			lastDate = line;
			
			continue;
		}
		
		// time entry
		
		let minutes = getExplicitMinutes(line);
		
		line = line.replace(/ = [\dhm]+$/, "");
		
		let startTime;
		let endTime = null;
		let description;
		let subtractMinutes = null;
		
		if (line.match(/\(-\d+\)$/)) {
			let [, s] = line.match(/\(-(\d+)\)$/);
			
			subtractMinutes = Number(s);
			
			line = line.replace(/\s*\(-\d+\)$/, "");
		}
		
		if (line.match(/^\d+\s*-\s*\d+$/)) {
			line = line.replace(/\s*-\s*/, " - ");
		}
		
		if (line.match(/^\d+\s*-\s*\d+/)) { // start and end time at the beginning
			let [, s, e] = line.match(/^(\d+)\s*-\s*(\d+)/);
			
			if (s.length === 2) {
				s = hour + s;
			}
			
			hour = s.substr(0, 2);
			
			if (e.length === 2) {
				e = hour + e;
			}
			
			startTime = s;
			endTime = e;
		} else {
			if (line.match(/^\d+/)) {
				let [, s] = line.match(/^(\d+)/);
				
				if (s.length === 2) {
					s = hour + s;
				}
				
				hour = s.substr(0, 2);
				startTime = s;
			}
			
			if (line.match(/ \d+$/)) {
				let [, e] = line.match(/(\d+)$/);
				
				if (e.length === 2) {
					e = hour + e;
				}
				
				endTime = e;
			}
		}
		
		description = line.replace(/^\d+ - \d+/, "").replace(/^\d+\s+/, "").replace(/\s+-\s+\d+$/, "");
		
		if (line.match(/^\d+$/)) {
			description = "(cont.)";
		}
		
		if (!startTime) {
			startTime = lastEntryEndTime;
		}
		
		if (!endTime) {
			// (in progress)
			
			let now = new Date();
			let h = now.getHours();
			let m = now.getMinutes();
			
			endTime = pad(h) + pad(m);
			
			description += " (in progress)";
		}
		
		if (minutes === null) {
			let startHour = Number(startTime.substr(0, 2));
			let startMinute = Number(startTime.substr(2));
			let endHour = Number(endTime.substr(0, 2));
			let endMinute = Number(endTime.substr(2));
			
			let startPoint = startHour * 60 + startMinute;
			let endPoint = endHour * 60 + endMinute;
			
			minutes = endPoint - startPoint;
		}
		
		if (!description || description === "cont") {
			description = "(cont.)";
		}
		
		entries.push({
			date,
			startTime,
			endTime,
			description,
			minutes,
		});
		
		lastEntryEndTime = endTime;
		hour = lastEntryEndTime.substr(0, 2);
	}
	
	let groupedEntries = groupBy(entries, "date");
	
	for (let [date, entries] of Object.entries(groupedEntries)) {
		groupedEntries[date] = {
			entries,
			total: entries.reduce((acc, entry) => acc + entry.minutes, 0),
		};
	}
	
	return {
		firstDate,
		lastDate,
		days: Object.keys(groupedEntries).length,
		groupedEntries,
	};
}
