---
title: Time
---

<script>
import {onMount} from "svelte";
import {getGroupedEntries, formatTime, formatMinutes, pad} from "./time";

let lines = `
6/2

1123-39 added comment to PR #123 (ABC-1234)

`.split("\n").filter(Boolean).map(s => s.trim());

let {firstDate, lastDate, days, groupedEntries} = getGroupedEntries(lines);

let now = new Date();
let currentDate = now.getDate() + "/" + (now.getMonth() + 1);

let showToday = localStorage.getItem("time.showToday") === "1";
let selectedDate = null;

$: localStorage.setItem("time.showToday", showToday ? "1" : "");

$: total = Object.values(groupedEntries).reduce(function(acc, {total}) {
	return acc + total;
}, 0);

$: average = total / Object.keys(groupedEntries).length;

onMount(function() {
	setTimeout(function() {
		document.documentElement.scrollTop = document.documentElement.scrollHeight;
	}, 200);
});
</script>

# Time Log

## XYZ Project

<div id="controls">
	<label>
		<input type="checkbox" bind:checked={showToday}>
		Show today
	</label>
</div>

{#each Object.entries(groupedEntries).filter(([date]) => (showToday || date !== currentDate || Object.keys(groupedEntries).length === 1) && (!selectedDate || date === selectedDate)) as [date, {entries, total}]}
	<h3 on:click={() => selectedDate = date}>{date}</h3>
	
	<table>
		<colgroup>
			<col width="70"/>
			<col width="70"/>
			<col/>
			<col width="90"/>
		</colgroup>
		<thead>
			<tr>
				<th>Start</th>
				<th>End</th>
				<th>Description</th>
				<th>Hours</th>
			</tr>
		</thead>
		<tbody>
			{#each entries as {startTime, endTime, description, minutes}}
				<tr>
					<td>{formatTime(startTime)}</td>
					<td>{formatTime(endTime)}</td>
					<td>{description}</td>
					<td>{formatMinutes(minutes)}</td>
				</tr>
			{/each}
			<tr class="footer">
				<!--<td colspan="2"></td>-->
				<td colspan="3">Total</td>
				<td>{formatMinutes(total)}</td>
			</tr>
		</tbody>
	</table>
{/each}

<h3>Summary ({firstDate} - {lastDate}, {days} days)</h3>

<table>
	<colgroup>
		<col/>
		<col width="90"/>
	</colgroup>
	<thead>
		<tr>
			<th>Description</th>
			<th>Hours</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td>Total</td>
			<td>{formatMinutes(total)}</td>
		</tr>
		<tr>
			<td>Average</td>
			<td>{formatMinutes(average)}</td>
		</tr>
	</tbody>
</table>

<br>

<p align="center">
	Example Software Ltd
	<br>
	123 Main Street, London A1 2BC
</p>
