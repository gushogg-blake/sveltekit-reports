import {json} from "@sveltejs/kit";
import pages from "$lib/pages.server";

export async function GET({params}) {
	return json(pages.bySlug[params.slug]?.importPath || null);
}
