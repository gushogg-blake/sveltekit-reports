import {json} from "@sveltejs/kit";
import pages from "$lib/pages.server";

export async function GET() {
	return json(pages.all.map(function(page) {
		return {...page, content: undefined};
	}));
}
