import {error} from "@sveltejs/kit";
import {dev} from "$app/environment";
import loadPage from "$lib/loadPage";
import createPage from "$lib/createPage";

export async function load({fetch, params}) {
	let path = decodeURIComponent(params.path).replace(/\/$/, "");
	let slug = path.split("/").at(-1);
	let page = await loadPage(slug, fetch);
	
	if (page.draft && !dev) {
		throw error(404, {
			message: "Page not found",
		});
	}
	
	return {
		page,
	};
}
