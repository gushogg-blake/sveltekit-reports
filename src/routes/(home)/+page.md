<script>
import moment from "moment";
import groupBy from "$utils/array/groupBy";

export let data;

let {pages} = data;

let reports = pages.all();
</script>

<style lang="scss">
h3 {
	font-size: 1.08em;
}

h4 {
	font-size: .95em;
}

li {
	margin-bottom: 1em;
}
</style>

<ul>
	{#each reports as {href, link, title, subtitle, date}}
		<li class="post">
			<div class="title">
				<a href={href}>
					{link || title + (subtitle ? ": " + subtitle : "")}
				</a>
			</div>
		</li>
	{/each}
</ul>
