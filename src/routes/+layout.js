import loadSite from "$lib/loadSite";

export let ssr = false;

export async function load({fetch}) {
	return {
		pages: await loadSite(fetch),
	};
}
